from discord.ext import commands
import discord
import os
import asyncio
import shelve

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='%falcxilulo% ', intents=intents)
mondSuperPerm = shelve.open("super-perm")

async def fail(knt):
    await knt.reply("ЖАЛКИЙ СМЕРТНЫЙ! ТЫ НЕ СПОСОБЕН ОБУЗДАТЬ МОЩЬ ЭТОЙ КОМАНДЫ! МУАХАХА!", ephemeral=True)

def setSuperPerm(knt, val):
    global mondSuperPerm
    mondSuperPerm[str(knt.guild.id)] = val
def getSuperPerm(knt):
    global mondSuperPerm
    return mondSuperPerm.get(str(knt.guild.id)) or set()

@bot.hybrid_command(description='Удалить сообщения, начинающиеся на "((" или "//"')
async def cc(knt):
    series = False
    async def f(msg, cond):
        nonlocal series
        if cond:
            series = True
            await msg.delete()
        elif series:
            return True
    await cc_real(knt, f)

@bot.hybrid_command(name='cc-all', description='Удалить сообщения, начинающиеся на "((" или "//", среди последних ста')
async def cc_all(knt):
    async def f(msg, cond):
        if cond:
            await msg.delete()
    await cc_real(knt, f)

async def cc_real(knt, f):
    if knt.guild is None:
        return
    super = getSuperPerm(knt)
    if not (knt.author.guild_permissions.manage_messages or any(map(lambda x: x.id in super, knt.author.roles))):
        await fail(knt)
        return
    await knt.defer(ephemeral=True)
    async for msg in knt.history():
        if await f(msg, msg.content.startswith("((") or msg.content.startswith("//")):
            break
    await knt.reply("Ага.", ephemeral=True, delete_after=1)

@bot.hybrid_group(name="cc-perm", fallback="list", description="Показать список ролей, имеющих право на использование /cc")
async def cc_perm(knt):
    if knt.guild is None:
        return
    super = getSuperPerm(knt)
    roloj = list(filter(lambda x: x.id in super, await knt.guild.fetch_roles()))
    setSuperPerm(knt, set(map(lambda x: x.id, roloj)))
    roloj = list(map(lambda x: str(x), roloj))
    tekst = "Этой суперсилой владеют "
    if len(roloj) == 0:
        tekst = "Пока что эту суперсилу получили только модераторы. `/cc-perm allow [роль]`, чтобы это разнообразить"
    elif len(roloj) >= 3:
        tekst += ", ".join(roloj[:-1]) + ", ну и, конечно же, " + roloj[-1]
    else:
        tekst += " и ".join(roloj)
    await knt.reply(tekst+".")

async def cc_set(knt, rol: discord.Role, f, tekst):
    if knt.guild is None:
        return
    servilp = knt.author.guild_permissions
    if not (servilp.manage_roles and servilp.manage_messages):
        await fail(knt)
        return
    super = getSuperPerm(knt)
    super2 = f(super, {rol.id})
    if super2 == super:
        await knt.reply("Уже.")
    else:
        setSuperPerm(knt, super2)
        await knt.reply(f"{str(rol)} {tekst}." )
    
@cc_perm.command(name="allow", description="Даровать роли суперсилу на использование /cc")
async def cc_allow(knt, rol: discord.Role):
    await cc_set(knt, rol, lambda x, y: x | y, "даровали суперсилу! Теперь они могут использовать `/cc`")

@cc_perm.command(name="disallow", description="Отобрать у роли суперсилу /cc")
async def cc_disallow(knt, rol: discord.Role):
    await cc_set(knt, rol, lambda x, y: x - y, "обворовали на суперсилу: никаких `/cc` вам больше")
  
bot.run(open(os.getenv("TOKEN") or "token", "r").readline())
