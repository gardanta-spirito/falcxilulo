{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; }; 
          python = pkgs.python3.withPackages (p: with p; [ discordpy ]); in
      {
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = [ python ];
        };
        nixosModules.default = { config, ... }: {
          options = {
            falcxilulo.tokenFile = pkgs.lib.mkOption { type = pkgs.lib.types.path; };
          };
          config = {
            users.groups."falcxilulo" = {};
            users.users."falcxilulo" = {
              isSystemUser = true;
              group = "falcxilulo";
            };
            systemd.services.discord-falcxilulo = {
              description = "Roboto por Discord";
              after = [ "network-online.target" ];
              wantedBy = [ "multi-user.target" ];
              serviceConfig = {
                Environment="TOKEN=${config.falcxilulo.tokenFile}";
                ExecStart = "${python}/bin/python3 ${./apl.py}";
                User = "falcxilulo";
                StateDirectory = "falcxilulo";
                WorkingDirectory = "/var/lib/falcxilulo";
                Restart = "on-failure";
              };
            };
          };
        };
      }
    );
}